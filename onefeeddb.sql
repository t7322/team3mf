-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Vært: 127.0.0.1
-- Genereringstid: 22. 05 2022 kl. 11:59:37
-- Serverversion: 10.4.22-MariaDB
-- PHP-version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onefeed`
--

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `socials`
--

CREATE TABLE `socials` (
  `id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_danish_ci NOT NULL,
  `api_url` varchar(80) CHARACTER SET ascii DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

--
-- Data dump for tabellen `socials`
--

INSERT INTO `socials` (`id`, `name`, `api_url`) VALUES
(1, 'Twitter', 'https://api.twitter.com/2'),
(2, 'Facebook', NULL);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstName` varchar(32) COLLATE utf8_danish_ci NOT NULL,
  `lastName` varchar(69) COLLATE utf8_danish_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_danish_ci NOT NULL,
  `password` text COLLATE utf8_danish_ci NOT NULL,
  `refresh_token` text COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

--
-- Data dump for tabellen `users`
--

INSERT INTO `users` (`id`, `firstName`, `lastName`, `email`, `password`, `refresh_token`) VALUES
(4, 'rane', 'arnesen', 'rane@mail.dk', '$2b$04$9N2d5eofDQP1XyGMfZtTT.5e/Jme9nU19o23kvpVY2JQFtazrCyyq', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InJhbmVAbWFpbC5kayIsImlhdCI6MTY1MTkxNTYzM30.FEceZNpJUy97s_4Aw-QoLCB_CaPypxgvR74c8EJclWg');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `user_socials`
--

CREATE TABLE `user_socials` (
  `id` int(11) NOT NULL,
  `social_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `social_user_id` varchar(255) COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

--
-- Data dump for tabellen `user_socials`
--

INSERT INTO `user_socials` (`id`, `social_id`, `user_id`, `social_user_id`) VALUES
(133, 1, 4, '4166842689');

--
-- Begrænsninger for dumpede tabeller
--

--
-- Indeks for tabel `socials`
--
ALTER TABLE `socials`
  ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `user_socials`
--
ALTER TABLE `user_socials`
  ADD PRIMARY KEY (`id`);

--
-- Brug ikke AUTO_INCREMENT for slettede tabeller
--

--
-- Tilføj AUTO_INCREMENT i tabel `socials`
--
ALTER TABLE `socials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tilføj AUTO_INCREMENT i tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Tilføj AUTO_INCREMENT i tabel `user_socials`
--
ALTER TABLE `user_socials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
